def df3(C, slope):
    xy_fall  = (0.96, 1.66) if slope == 0.05 else (0.96, 1.84)
    xy0_fall = (0.003, 0.61) if slope == 0.05 else (0.003, 0.78)

    xy_rise  = (0.96, 2.08) if slope == 0.05 else (0.96, 2.26)
    xy0_rise = (0.003, 0.53) if slope == 0.05 else (0.003, 0.71)

    m_fall = (xy_fall[1] - xy0_fall[1]) / (xy_fall[0] - xy0_fall[0])
    m_rise = (xy_rise[1] - xy0_rise[1]) / (xy_rise[0] - xy0_rise[0])

    delay_rise = m_rise * (C - xy0_rise[0]) + xy0_rise[1]
    delay_fall = m_fall * (C - xy0_fall[0]) + xy0_fall[1]

    return max(delay_fall, delay_rise)

def nand22(C, ab, slope):
    if ab == 'a':
        xy_fall  = (0.64, 0.83) if slope == 0.05 else (0.64, 1.08)
        xy0_fall = (0.002, 0.03) if slope == 0.05 else (0.002, -0.11)

        xy_rise  = (0.64, 1.62) if slope == 0.05 else (0.64, 2.00)
        xy0_rise = (0.002, 0.05) if slope == 0.05 else (0.002, 0.35)

        m_fall = (xy_fall[1] - xy0_fall[1]) / (xy_fall[0] - xy0_fall[0])
        m_rise = (xy_rise[1] - xy0_rise[1]) / (xy_rise[0] - xy0_rise[0])

        delay_rise = m_rise * (C - xy0_rise[0]) + xy0_rise[1]
        delay_fall = m_fall * (C - xy0_fall[0]) + xy0_fall[1]

        return max(delay_fall, delay_rise)

    elif ab == 'b':
        xy_fall  = (0.64, 0.84) if slope == 0.05 else (0.64, 0.93)
        xy0_fall = (0.002, 0.03) if slope == 0.05 else (0.002, -0.15)

        xy_rise  = (0.64, 1.64) if slope == 0.05 else (0.64, 2.04)
        xy0_rise = (0.002, 0.07) if slope == 0.05 else (0.002, 0.43)

        m_fall = (xy_fall[1] - xy0_fall[1]) / (xy_fall[0] - xy0_fall[0])
        m_rise = (xy_rise[1] - xy0_rise[1]) / (xy_rise[0] - xy0_rise[0])

        delay_rise = m_rise * (C - xy0_rise[0]) + xy0_rise[1]
        delay_fall = m_fall * (C - xy0_fall[0]) + xy0_fall[1]

        return max(delay_fall, delay_rise)

if __name__ == '__main__':
    print "SLOPE = 0.05 ns {"
    delay = nand22(0.005, 'a', 0.05) + df3(0.008, 0.05)
    print "\tFREQUENCY = %.3f" %(delay**-1), "GHz"
    print "\tPERIOD = %.3f" %(delay), "ns"
    print "}"

    print "\nSLOPE = 2.00 ns {"
    delay = nand22(0.005, 'a', 2.00) + df3(0.008, 2.00)
    print "\tFREQUENCY = %.3f" %(delay**-1), "GHz"
    print "\tPERIOD = %.3f" %(delay), "ns"
    print "}"

    print "\nSLOPE = 0.05 ns (set-up time 0.1ns) {"
    delay = nand22(0.005, 'a', 0.05) + df3(0.008, 0.05) + 0.1
    print "\tFREQUENCY = %.3f" %(delay**-1), "GHz"
    print "\tPERIOD = %.3f" %(delay), "ns"
    print "}"

    print "\nSLOPE = 2.00 ns  (set-up time 0.1ns) {"
    delay = nand22(0.005, 'a', 2.00) + df3(0.008, 2.00) + 0.1
    print "\tFREQUENCY = %.3f" %(delay**-1), "GHz"
    print "\tPERIOD = %.3f" %(delay), "ns"
    print "}"


