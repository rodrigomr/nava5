def df1(C, slope):
    xy_fall  = (0.001, 0.56) if slope == 0.05 else (0.001, 0.73)
    xy0_fall = (0.32, 1.62) if slope == 0.05 else (0.32, 1.79)

    xy_rise  = (0.001, 0.5) if slope == 0.05 else (0.001, 0.67)
    xy0_rise = (0.32, 2.14) if slope == 0.05 else (0.32, 2.31)

    m_fall = (xy_fall[1] - xy0_fall[1]) / (xy_fall[0] - xy0_fall[0])
    m_rise = (xy_rise[1] - xy0_rise[1]) / (xy_rise[0] - xy0_rise[0])

    delay_rise = m_rise * (C - xy0_rise[0]) + xy0_rise[1]
    delay_fall = m_fall * (C - xy0_fall[0]) + xy0_fall[1]

    return max(delay_fall, delay_rise)

def nor23(C, ab, slope):
    if ab == 'a':
        xy_fall  = (0.003, 0.06) if slope == 0.05 else (0.003, 0.14)
        xy0_fall = (0.96, 1.06) if slope == 0.05 else (0.96, 1.35)

        xy_rise  = (0.003, 0.07) if slope == 0.05 else (0.003, 0.18)
        xy0_rise = (0.96, 1.60) if slope == 0.05 else (0.96, 1.78)

        m_fall = (xy_fall[1] - xy0_fall[1]) / (xy_fall[0] - xy0_fall[0])
        m_rise = (xy_rise[1] - xy0_rise[1]) / (xy_rise[0] - xy0_rise[0])

        delay_rise = m_rise * (C - xy0_rise[0]) + xy0_rise[1]
        delay_fall = m_fall * (C - xy0_fall[0]) + xy0_fall[1]

        return max(delay_fall, delay_rise)

    elif ab == 'b':
        xy_fall  = (0.003, 0.04) if slope == 0.05 else (0.003, 0.04)
        xy0_fall = (0.96, 1.04) if slope == 0.05 else (0.96, 1.33)

        xy_rise  = (0.003, 0.05) if slope == 0.05 else (0.003, 0.24)
        xy0_rise = (0.96, 1.58) if slope == 0.05 else (0.96, 1.90)

        m_fall = (xy_fall[1] - xy0_fall[1]) / (xy_fall[0] - xy0_fall[0])
        m_rise = (xy_rise[1] - xy0_rise[1]) / (xy_rise[0] - xy0_rise[0])

        delay_rise = m_rise * (C - xy0_rise[0]) + xy0_rise[1]
        delay_fall = m_fall * (C - xy0_fall[0]) + xy0_fall[1]

        return max(delay_fall, delay_rise)

def nand23(C, ab, slope):
    if ab == 'a':
        xy_fall  = (0.003, 0.03) if slope == 0.05 else (0.003, -0.13)
        xy0_fall = (0.96, 0.83) if slope == 0.05 else (0.96, 1.08)

        xy_rise  = (0.003, 0.04) if slope == 0.05 else (0.003, 0.34)
        xy0_rise = (0.96, 1.67) if slope == 0.05 else (0.96, 2.03)

        m_fall = (xy_fall[1] - xy0_fall[1]) / (xy_fall[0] - xy0_fall[0])
        m_rise = (xy_rise[1] - xy0_rise[1]) / (xy_rise[0] - xy0_rise[0])

        delay_rise = m_rise * (C - xy0_rise[0]) + xy0_rise[1]
        delay_fall = m_fall * (C - xy0_fall[0]) + xy0_fall[1]

        return max(delay_fall, delay_rise)

    elif ab == 'b':
        xy_fall  = (0.003, 0.04) if slope == 0.05 else (0.003, -0.15)
        xy0_fall = (0.96, 0.85) if slope == 0.05 else (0.96, 0.94)

        xy_rise  = (0.003, 0.08) if slope == 0.05 else (0.003, 0.46)
        xy0_rise = (0.96, 1.70) if slope == 0.05 else (0.96, 2.07)

        m_fall = (xy_fall[1] - xy0_fall[1]) / (xy_fall[0] - xy0_fall[0])
        m_rise = (xy_rise[1] - xy0_rise[1]) / (xy_rise[0] - xy0_rise[0])

        delay_rise = m_rise * (C - xy0_rise[0]) + xy0_rise[1]
        delay_fall = m_fall * (C - xy0_fall[0]) + xy0_fall[1]

        return max(delay_fall, delay_rise)

if __name__ == '__main__':
    print "SLOPE = 0.05 ns {"
    C_PAR = 0.013 + 0.021
    delay_nand = nand23(0.005, 'a', 0.05) + df1(0.020, 0.05)
    delay_nor = nor23(0.005, 'a', 0.05) + df1(C_PAR, 0.05)

    delay = None
    if delay_nand > delay_nor:
        print "\tNAND"
        delay = delay_nand
    else:
        print "\tNOR"
        delay = delay_nor

    print "\tFREQUENCY = %.3f" %(delay**-1), "GHz"
    print "\tPERIOD = %.3f" %(delay), "ns"
    print "}"

    print "\nSLOPE = 2.00 ns {"
    C_PAR = 0.013 + 0.021
    delay_nand = nand23(0.005, 'a', 2.00) + df1(0.020, 2.00)
    delay_nor = nor23(0.005, 'a', 2.00) + df1(C_PAR, 2.00)

    delay = None
    if delay_nand > delay_nor:
        print "\tNAND"
        delay = delay_nand
    else:
        print "\tNOR"
        delay = delay_nor

    print "\tFREQUENCY = %.3f" %(delay**-1), "GHz"
    print "\tPERIOD = %.3f" %(delay), "ns"
    print "}"

    print "\nSLOPE = 0.05 ns (set-up time 0.1ns) {"
    C_PAR = 0.013 + 0.021
    delay_nand = nand23(0.005, 'a', 0.05) + df1(0.020, 0.05) + 0.1
    delay_nor = nor23(0.005, 'a', 0.05) + df1(C_PAR, 0.05) + 0.1

    delay = None
    if delay_nand > delay_nor:
        print "\tNAND"
        delay = delay_nand
    else:
        print "\tNOR"
        delay = delay_nor

    print "\tFREQUENCY = %.3f" %(delay**-1), "GHz"
    print "\tPERIOD = %.3f" %(delay), "ns"
    print "}"

    print "\nSLOPE = 2.00 ns  (set-up time 0.1ns) {"
    C_PAR = 0.013 + 0.021
    delay_nand = nand23(0.005, 'a', 2.00) + df1(0.020, 2.00) + 0.1
    delay_nor = nor23(0.005, 'a', 2.00) + df1(C_PAR, 2.00) + 0.1

    delay = None
    if delay_nand > delay_nor:
        print "\tNAND"
        delay = delay_nand
    else:
        print "\tNOR"
        delay = delay_nor

    print "\tFREQUENCY = %.3f" %(delay**-1), "GHz"
    print "\tPERIOD = %.3f" %(delay), "ns"
    print "}"


